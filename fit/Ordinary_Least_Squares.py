# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from sklearn import datasets, linear_model

train_data = pd.read_csv("../data/training_data_2014.csv")
test_data = pd.read_csv("../data/test_data_2015.csv")
X_train=train_data.loc[:,["W3","W4","W5"]]
y_train=train_data.loc[:,["W6"]]
X_test=test_data.loc[:,["W3","W4","W5"]]
regr = linear_model.Ridge(alpha=0.05,fit_intercept=False)
# Train the model using the training sets
regr.fit(X_train, y_train)

print np.concatenate((regr.predict(X_train),regr.predict(X_test)), axis=1)
# The coefficients
print('Coefficients: \n', regr.coef_)
# The intercept
print('Intercept: \n', regr.intercept_)
# The mean square error
print("Residual sum of squares: %.2f"
      % np.mean((regr.predict(X_train) - y_train) ** 2))

